import { createRouter, createWebHistory } from "vue-router";
import { PAGE_TITLE } from "@/constants";

const Light = () => import("../views/Light.vue");
const LightOklch = () => import("../views/LightOklch.vue");
const Dark = () => import("../views/Dark.vue");
const DarkOklch = () => import("../views/DarkOklch.vue");

export const routes = [
  {
    path: "/",
    name: "light",
    component: Light,
    meta: {
      title: "Light",
    },
  },
  {
    path: "/light-oklch",
    name: "light-oklch",
    component: LightOklch,
    meta: {
      title: "Light (oklch)",
    },
  },
  {
    path: "/dark",
    name: "dark",
    component: Dark,
    meta: {
      title: "Dark",
    },
  },
  {
    path: "/dark-oklch",
    name: "dark-oklch",
    component: DarkOklch,
    meta: {
      title: "Dark (oklch)",
    },
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

router.beforeEach((to) => {
  document.title = [to.meta?.title, PAGE_TITLE].filter(Boolean).join(" - ");
});

export default router;

export const arrayToPalette = (arr: Array) => {
  return {
    50: { $value: arr[0] },
    100: { $value: arr[1] },
    200: { $value: arr[2] },
    300: { $value: arr[3] },
    400: { $value: arr[4] },
    500: { $value: arr[5] },
    600: { $value: arr[6] },
    700: { $value: arr[7] },
    800: { $value: arr[8] },
    900: { $value: arr[9] },
    950: { $value: arr[10] },
  };
};

export const slugify = (input: string) => {
  const a =
    "àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;";
  const b =
    "aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------";
  const p = new RegExp(a.split("").join("|"), "g");

  return input
    .toString()
    .toLowerCase()
    .replace(/\s+/g, "-") // Replace spaces with -
    .replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special characters
    .replace(/&/g, "-and-") // Replace & with 'and'
    .replace(/[^\w-]+/g, "") // Remove all non-word characters
    .replace(/--+/g, "-") // Replace multiple - with single -
    .replace(/^-+/, "") // Trim - from start of text
    .replace(/-+$/, ""); // Trim - from end of text
};
